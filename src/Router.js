import Component from './components/Component.js';

export default class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		this.routes.forEach(element => {
			if (element.path == path) {
				const c = new Component('h1', null, element.title);
				this.titleElement.innerHTML = c.render();
				this.contentElement.innerHTML = element.page.render();
			}
		});
	}
}
