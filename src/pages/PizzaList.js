import PizzaThumbnail from '../components/PizzaThumbnail.js';
import Component from '../components/Component.js';

export default class PizzaList extends Component {
	constructor(pizzaList) {
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			pizzaList.map(pizza => {
				return new PizzaThumbnail(pizza);
			})
		);
	}
}
