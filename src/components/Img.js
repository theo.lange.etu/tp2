import Component from './Component.js';

export default class Img extends Component {
	constructor(value) {
		super('img', { name: 'src', value: value }, null);
	}
	render() {
		return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}"/>`;
	}
}
