export default class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (this.children) {
			return `<${
				this.tagName
			} ${this.renderAttribute()}>${this.renderChildren()}</${this.tagName}>`;
		}
		return `<${this.tagName} ${this.renderAttribute()}"/>`;
	}

	renderAttribute() {
		if (this.attribute) {
			return `${this.attribute.name}="${this.attribute.value}"`;
		} else {
			return '';
		}
	}

	renderChildren() {
		if (Array.isArray(this.children)) {
			return this.children.reduce((acc, child) => {
				return acc + this.renderChild(child);
			}, '');
		} else {
			return this.renderChild(this.children);
		}
	}

	renderChild(child) {
		if (child instanceof Component) {
			return child.render();
		} else {
			return child;
		}
	}
}
